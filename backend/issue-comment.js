const express = require('express')
const axios = require('axios')
const cors = require('cors');
const app = express()

async function getGitHubIssueComment(){ 
    const res = await axios({
        url: 'https://api.github.com/graphql',
        method: 'post',
        headers: {
            'Content-Type': 'application/json',
            Accept: 'application/json',
            Authorization: 'bearer 02565b8874d4426bcf60a917bb3c55980435a15f'
        },
        data: {
            query: `
        {
            repository(name:"react", owner:"facebook"){
                id
                name
                issue(number: 16062){
                    comments(last: 5){
                        nodes{
                            author{
                                login
                            }
                            body
                        }
                    }
                }
            }
        }`
        }
    })
    console.log(res.data.data.repository.issues)
    return res.data.data.repository.issues

}
//resolver
var root = { 
    async commentIssues() {
        return await getGitHubIssueComment()
    }
}
app.use(cors());

app.post('/comment', (req, res) => {
    console.log("Mencoba Comment")
    root.commentIssues()
        .then(response => {
            res.json(response)
        })
    //res.send('HAI');
})

app.listen("8080", () => {
    console.log("Mini project run on port 8080")
})